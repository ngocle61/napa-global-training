const express = require('express')

const authController = require('../modules/auth/auth-controller')
const { statusAuth } = require('../modules/auth/auth-middleware')
const usersCtrl = require('../modules/user/user-controller')


const loginRouter = express.Router();

const registerRouter = express.Router();

const forgotPasswordRouter = express.Router();

const changePasswordRouter = express.Router();

//register

registerRouter.post("/", usersCtrl.createUser);


forgotPasswordRouter.post("/", authController.postForgotPassword);
changePasswordRouter.put("/:token", authController.putChangePassword);

loginRouter.post("/", statusAuth, authController.postLogin);


module.exports = {
    loginRouter,
    registerRouter,
    forgotPasswordRouter,
    changePasswordRouter
}