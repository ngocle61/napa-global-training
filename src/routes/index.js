const express = require('express');
const { loginRouter, registerRouter, forgotPasswordRouter, changePasswordRouter } = require('./auth-route')
const apiRouter = express.Router();

apiRouter.use("/login", loginRouter);
apiRouter.use("/register", registerRouter);
apiRouter.use("/change-password", changePasswordRouter);
apiRouter.use("/forgot-password", forgotPasswordRouter);


module.exports = apiRouter;
