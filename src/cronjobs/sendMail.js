const moment = require("moment");
var cron = require("node-cron");
const { sendMail } = require("../modules/auth/auth-services");

const sendMailCronJob = () => {
  const from = "nap global";
  const to = "ngochq0601@gmail.com";
  const subject = "message";
  const content = "content";
  cron.schedule("* * * * *", () => {
    var mainOptions = {
      from: from,
      to: to,
      subject: subject,
      content: content,
      date: moment(new Date()).format("LT"),
      html: "<b>Hello world </b>",
    };
    sendMail(mainOptions);
  });
};

module.exports = sendMailCronJob;
