const mongoose = require("mongoose");
const moment = require("moment");

const formatText = mess => mess.trim() == "" ? false : mess.trim();

const formatMessage = mess => {
    return {
        ...mess,
        createdAt: moment(mess.createdAt).format('LT'),
    };
}

function ChatManager(io) {
    const ChatModel = mongoose.model("Chat");

    async function getAllMessages({ userId }, cb, limit = 20) {
        const allMessages = await ChatModel.find()
            .populate('user', 'username')
            .sort({
                createdAt: -1
            })
            .limit(limit).exec();
        cb({
            allMessages: allMessages.map(item => {
                return formatMessage({
                    user: item.user,
                    message: item.message,
                    createdAt: item.createdAt
                })
            })
        });
    }

    const addNew = () => async (data, cb) => {
        if (formatText(data.message)) {
            try {
                const newChat = new ChatModel({
                    user: data.userId,
                    message: formatText(data.message)
                })
                await newChat.save();
                const chat = await ChatModel.findById(newChat._id).populate('user', 'username').exec();
                io.emit("new-message", formatMessage(chat.toJSON()));
                cb(true);
            } catch (er) {
                console.log(er);
            }
        }
    }

    return {
        getAllMessages,
        addNew,
    }
}


const boxChat = (io) => {
    const _ChatManager = ChatManager(io);

    const listen = (clientSocket) => {
        clientSocket.emit("connected");
        clientSocket.on("all-messages", _ChatManager.getAllMessages);
        clientSocket.on("new-message", _ChatManager.addNew());
    }

    io.on("connection", (clientSocket) => {
        listen(clientSocket);
    })

}



module.exports = boxChat