const testChat = require("./test-chat");
const chat = require("./chat");

module.exports = {
    testChat,
    chat,
}