const {
    testChat,
} = require("./namespaces")



const _socket = (io) => {
    testChat(io.of("/test-chat"));
}

module.exports = _socket;