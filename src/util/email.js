const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: "ngoc",
  port: 3001,
  secure: false,
  auth: {
    user: "ngoc@gmail.com",
    pass: "ngoc123123",
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false,
  },
});

const createContent = (token) => `
        <div style="padding: 10px; background-color: #003375">
            <div style="padding: 10px; background-color: white;">
                <h4 style="color: #0085ff">Napa global training forgot password</h4>
                <a href="abc">changepassword/${token}</a>
            </div>
        </div>
    `;

const createOptions = (receiver, html) => ({
  from: "Napa global training",
  subject: "Napa global training password",
  to: receiver,
  html,
});

const sendMailForgotPassword = (token, receiver) =>
  new Promise((rs, rj) => {
    try {
      transporter.sendMail(
        createOptions(receiver, createContent(token)),
        (err, info) => {
          if (err) rj(err);
          else rs(info.response);
        }
      );
    } catch (error) {
      rj(error);
    }
  });

module.exports = { sendMailForgotPassword };
