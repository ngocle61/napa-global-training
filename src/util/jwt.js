const jwt = require('jsonwebtoken')
// require('dotenv').config()

const generateToken = (dataInToken) => {
  return new Promise((resolve, reject) => {
    try {
      const token = jwt.sign(dataInToken, process.env.ACCESS_TOKEN_SECRET);
      resolve(token);
    } catch (err) {
      reject(err);
    }
  });
};

const verifyToken = (tokenFromClient) => {
  return new Promise((resolve, reject) => {
    try {
      const data = jwt.verify(tokenFromClient, process.env.ACCESS_TOKEN_SECRET);
      if (data) {
        resolve(data);
      }
    } catch (err) {
      reject(err);
    }
  });
};

module.exports = { generateToken, verifyToken };
