const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ChatSchema = new Schema({
    user: { type: Schema.Types.ObjectId, ref: 'users' },
    message: { type: String },
}, {
    timestamps: true,
});


module.exports = mongoose.model('Chat', ChatSchema);