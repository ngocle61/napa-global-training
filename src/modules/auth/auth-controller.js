const { requestError, requestSuccess } = require("../../util/response")
const { login, forgotPassword, changePassword } = require('./auth-services')

const postLogin = (req, res) => {
    login(req.body).then(requestSuccess(res)).catch(requestError(res));
};

const postForgotPassword = (req, res) => {
    forgotPassword(req.body).then(requestSuccess(res)).catch(requestError(res));
};

const putChangePassword = (req, res) => {
    changePassword(req.params.token, req.body)
        .then(requestSuccess(res))
        .catch(requestError(res));
};

module.exports = {
    postLogin,
    postForgotPassword,
    putChangePassword,
};