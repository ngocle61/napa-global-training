const User = require("../user/user-model");
const { generateToken, verifyToken } = require("../../util/jwt");
const { sendMailForgotPassword } = require("../../util/email");
const nodemailer = require("nodemailer");

const login = (body) =>
  new Promise(async (rs, rj) => {
    console.log(1111);
    try {
      const userData = body || {};
      const reqUser = new User({
        email: userData.email,
        password: userData.password,
      });
      User.findOne({
        email: userData.email,
      }).then(async (data) => {
        if (data !== null) {
          if (reqUser.password === data.password) {
            const tokenToClient = await generateToken({
              _id: data._id,
              email: data.email,
              username: data.username,
              role: data.role,
              status: data.status,
            });
            rs(tokenToClient);
          } else rj("Wrong username or password");
        } else rj("Wrong username or password");
      });
    } catch (error) {
      rj(error);
    }
  });

const forgotPassword = (body) =>
  new Promise(async (rs, rj) => {
    try {
      const { email } = body || {};
      const userEmail = await User.findOne({ email });
      if (!userEmail) rj("Not found!");
      else {
        generateToken({ dataInToken: userEmail })
          .then((token) => {
            sendMailForgotPassword(token, email)
              .then((msg) => rs(msg))
              .catch((err) => rj(err));
          })
          .catch((err) => rj(err));
      }
    } catch (error) {
      rj(error);
    }
  });

const changePassword = (token, body) =>
  new Promise(async (rs, rj) => {
    try {
      const { password } = body;
      const user = await verifyToken(token);

      const changedUser = await User.findOne({
        email: user.email,
      });
      if (!changedUser) rj("Error");
      else {
        User.updateOne({ _id: changedUser._id }, { password })
          .then(() => {
            rs("Success changed!");
          })
          .catch((err) => rj(err));
      }
    } catch (error) {
      rj(error);
    }
  });

const sendMail = async (mainOptions) => {
  let testAccount = await nodemailer.createTestAccount();

  const transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com",
    secureConnection: false,
    port: 587,
    auth: {
      user: testAccount.user,
      pass: testAccount.pass,
    },
  });

  console.log(transporter, "transporter ");

  return transporter.sendMail({ ...mainOptions }, (err, info) => {
    if (err) {
      console.log(err, "error");
    } else {
      console.log("Email sent successfully. " + info);
    }
  });
};

module.exports = {
  login,
  forgotPassword,
  changePassword,
  sendMail,
};
