const jwt = require('jsonwebtoken')
const { requestError, requestSuccess } = require("../../util/response")
const User = require('../user/user-model')
require('dotenv').config()

const userAuth = (req, res, next) => {
    const authHeader = req.header('Authorization')
    const token = authHeader && authHeader.split(' ')[1]

    if (!token) {
        return res.status(400).json({ success: false, message: "Access token not found" })
    }
    try {
        const decoded = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
        req.userId = decoded.userId
        if (user.role == "ADMIN") {
            next()
        } else {
            requestError(res, 401)("wrong token");
        }
    } catch (error) {
        requestError(res, 401)("wrong token")
    }
}

const statusAuth = async (
    req,
    res,
    next
) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        console.log(user);
        if (user.status === "ACTIVE") {
            next();
        } else {
            requestError(res, 400)("invalid email!");
        }
    } catch (err) {
        requestError(res, 401)("status not allow");
    }
};

module.exports = { userAuth, statusAuth }