const { requestError, requestSuccess } = require("../../util/response")

const UserServices = require('./user-services')

const createUser = (req, res) => {
    UserServices.createUser({
        ...req.body,
        role: req.body.role ? req.body.role : "USER",
        status: req.body.status ? req.body.status : "ACTIVE",
    })
        .then(async (data) => {
            requestSuccess(res)(data);
        })
        .catch(requestError(res));
};



const editUser = (req, res) => {
    if (req.user._id === req.params.id) {
        UserServices.editUser(req.params.id, req.body)
            .then(requestSuccess(res))
            .catch((errors) => {
                requestError(res)({
                    email: "Somethings went wrong!",
                });
            });
    } else {
        requestError(res)({
            id: "Wrong token",
        });
    }
};

const changePassword = (req, res) => {
    UserServices.changePassword(req.user._id, req.body)
        .then(requestSuccess(res))
        .catch(requestError(res));
}

module.exports = { createUser, editUser, changePassword };
