const User = require('./user-model')
const { generateToken } = require('../../util/jwt')

const createUser = (body) =>
    new Promise(async (rs, rj) => {
        try {
            const userData = body || {};
            await User.create({
                ...userData,
            });
            const tokenToClient = await generateToken({
                // _id: userData._id,
                email: userData.email,
                username: userData.username,
                role: userData.role,
                status : "ACTIVE"
            });

            const responseData = responseUserData(userData);
            rs({
                user: responseData,
                token: tokenToClient,
            });
        } catch (error) {
            rj(error);
        }
    });

const editUser = (id, body) =>
    new Promise(async (rs, rj) => {
        try {
            const userId = id;
            const userData = body || {};
            await User.updateOne({ _id: userId }, userData);
            const user = await User.findById(id);
            const responseData = responseUserData(user);
            rs(responseData);
        } catch (error) {
            rj(error);
        }
    });

const changePassword = async (id, body) =>
    new Promise(async (rs, rj) => {
        try {
            const user = await User.findOne({
                _id: id,
            });
            const reqUser = new User({
                username: user.username,
                password: body.oldPassword,
            });

            const result = await argon2.verify(user.password, reqUser.password)

            if (result) {
                User.updateOne({ _id: id }, { password: body.newPassword })
                    .then(() => {
                        rs("Success changed!");
                    })
                    .catch((err) => rj(err));
            } else {
                rj({ error: "wrong old password" });
            }
        } catch (error) {
            rj(error);
        }
    });


const deleteUser = (_id) =>
    new Promise(async (rs, rj) => {
        try {
            const user = await User.findById(_id);
            await User.findByIdAndDelete(_id);
            rs({
                user: {
                    _id,
                    role: user.role,
                },
            });
        } catch (error) {
            rj(error);
        }
    });

const responseUserData = (data) => {
    const { _id, username, email, role, status } = data
    return {
        _id,
        username,
        email,
        role,
        status,
    };
};

module.exports = { createUser, editUser, deleteUser, changePassword }