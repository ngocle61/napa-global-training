import UserServices from "../user-services";
import { requestSuccess, requestError } from "../../../util/response";

const createUserByAdmin = (req, res) => {

  UserServices.createUser({ ...req.body })
    .then(async (data) => {
      requestSuccess(res)(data);
    })
    .catch(requestError(res));
};

const getUserById = (req, res) => {
  UserServices.getUsersById(req.params._id)
    .then((data) => {
      requestSuccess(res)(data);
    })
    .catch(requestError(res));
};

const updateUserByAdmin = (req, res) => {
  UserServices.editUser(req.params?._id, req.body)
    .then(requestSuccess(res))
    .catch(requestError(res));
};

const deleteUserByAdmin = (req, res) => {
  UserServices.deleteUser(req.params?._id)
    .then(requestSuccess(res)({
      user: {
        _id: data.user._id,
      },
    }))
    .catch(requestError(res));

};

export default {
  createUserByAdmin,
  getUserById,
  updateUserByAdmin,
  deleteUserByAdmin,
};
