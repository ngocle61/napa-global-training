const express = require('express');
const apiRouter = require('./src/routes')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express();
const http = require("http").Server(app);
const io = require("socket.io")(http);

const mongoUrl = process.env.MONGODB_URI;
const socket = require("./src/socket");
const { sendMail } = require("./src/cronjobs/index")

const PORT = process.env.PORT || 3001


const listen = () => {
    new Promise((resolve, reject) => {
        http.listen(PORT, () => {
            console.log(`App is listening on port: ${PORT}`);
            sendMail();
            resolve();
        });
    });
}

const connect = () =>
    new Promise((resolve, reject) => {
        mongoose.connect(mongoUrl, {}, err => {
            if (err) throw err
            console.log('Connected to MongoDB!!!')
        })
    });

connect()
    .then(() => {
        socket(io);
        return true;
    })
    .then(listen())
    .catch(er => {
        console.log('connect server.js ERROR', er);
    });



// app.use(express.json())
app.use(bodyParser.urlencoded())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use("/api", apiRouter)
